import logo from "./logo.svg";
import "./App.css";

function App() {
  const onPrintHandler = () => {
    window.electronAPI.printComponent(
      {
        url: "https://zto-fulfil-storage-dev.s3-ap-southeast-1.amazonaws.com/2022_11/pdf/grn-received-5323ec62-8f13-4a34-a43d-c9dd6584de70_e48b4664-ec73-4799-b819-623c2ee60eb8.pdf?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA4JNNAGCC5AELFBBB%2F20221108%2Fap-southeast-1%2Fs3%2Faws4_request&X-Amz-Date=20221108T031939Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=6f5abd4fab8d24e0f57ff953221d9d16535c79650cc25ff9c726c3ac750ab24b",
      },
      (x) => {
        console.log("Main: ", x);
      }
    );
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <button onClick={onPrintHandler}>Print</button>
      </header>
    </div>
  );
}

export default App;
