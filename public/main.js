"use strict";

const { app, BrowserWindow, ipcMain } = require("electron");
const path = require("path");
const isDev = require("electron-is-dev");
const { download } = require("electron-dl");

const { print } = require("pdf-to-printer");

require("@electron/remote/main").initialize();

const fs = require("fs");

function createWindow() {
  const win = new BrowserWindow({
    width: 1280,
    height: 1000,
    useContentSize: true,
    fullscreen: !isDev,
    kiosk: true,
    titleBarOverlay: {
      symbolColor: "#3399cc",
    },
    alwaysOnTop: !isDev,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      preload: path.join(__dirname, "preload.js"),
    },
    icon: path.join(__dirname, "favicon.png"),
  });

  // win.loadURL('https://internal.one.egitech.vn/')
  win.loadURL("http://localhost:3000");
  // Open the DevTools.
  if (isDev) {
    win.webContents.openDevTools({ mode: "detach" });
  }
}

app.on("ready", createWindow);

app.on("window-all-closed", function () {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", function () {
  if (BrowserWindow.getAllWindows().length === 0) createWindow();
});

//handle print
ipcMain.handle("printComponent", async (_event, info) => {
  const savePath = await download(
    BrowserWindow.getFocusedWindow(),
    info.url
  ).then((value) => {
    console.log("value", value.getSavePath());

    return value.getSavePath();
  });

  // Print file
  await print(savePath, {
    silent: true,
    scale: "fit",
  });

  // Delete file after printed
  fs.unlinkSync(savePath);

  return "Printed!";
});
